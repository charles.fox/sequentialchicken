# SequentialChicken

Game theory model for autonomous vehicles from the papers,

C Fox, F Camara, G Markkula, R Romano, R Madigan, and N Merat (2018). When should the chicken cross the road?: Game theory for autonomous vehicle-human interactions. Proc. 4th International Conference on Vehicle Technology and Intelligent Transport Systems (VEHITS), 2018

F Camara, R Romano, G Markkula, R Madigan, N Merat, and C Fox  (2018).  Empirical game theory of pedestrian interaction for autonomous vehicles.  Proceedings of Measuring Behavior.

F Camara, S Cosar, N Bellotto, N Merat, and C Fox. (2018) Towards pedestrian-AV interaction: method for elucidating pedestrian preferences.  IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS 2018) Workshops


TODO we are working to maintain code from several other publications which use versions of the model and add them here.