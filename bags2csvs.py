

import os, re
import pdb
import multiprocessing, sys, subprocess


directory = "/home/charles/docs/rak/bags"



def system_call_1(fn_crossing, stop_event):
  stop_event.wait()
  cmd = "/opt/ros/humble/bin/ros2 bag play %s"%fn_crossing      #terminates at end of the bag
  print(cmd)
  subprocess.run([cmd], shell=True)

def system_call_2(fn_csv, stop_event):
  stop_event.wait()
  cmd = "/opt/ros/humble/bin/ros2 topic echo /chicken_game_info --csv | tee %s "%fn_csv     #goes on forever, unless we kill the process
  print(cmd)
  subprocess.run([cmd], shell=True)


for file in sorted(os.listdir(directory)):
    filename_day = os.fsdecode(file)
    dir_day = os.path.join(directory, filename_day)
    print(dir_day)
    for file in sorted(os.listdir(dir_day)):
        filename_subject = os.fsdecode(file)
        dir_subject = os.path.join(dir_day, filename_subject)
        print(dir_subject)
        
        for file in sorted(os.listdir(dir_subject)):
            filename_crossing = os.fsdecode(file)
            dir_crossing = os.path.join(dir_subject, filename_crossing)
            print(dir_crossing)
        
            for file in sorted(os.listdir(dir_crossing)):
                filename_crossing = os.fsdecode(file)
                fn_crossing = os.path.join(dir_crossing, filename_crossing)
                if fn_crossing[-3:] == "db3":
                    print(fn_crossing)
                    
                    short_filename_subject = re.sub(r"^.*-", "", filename_subject)
                    fn_csv = "csv/%s_%s_%s"%(filename_day, short_filename_subject, filename_crossing)        
                    fn_csv = fn_csv[:-6]+".csv"
                    print(fn_csv)
                    stop_event = multiprocessing.Event()
                    proc1 = multiprocessing.Process(target=system_call_1, args=(fn_crossing, stop_event))
                    proc1.start()
                    proc2 = multiprocessing.Process(target=system_call_2, args=(fn_csv, stop_event))
                    proc2.start()
                    stop_event.set()   #unblock them both together

                    proc1.join()     #wait for the rosbag to complete playing
                    print("1 done")
                    proc2.terminate()    #kill the csv recorder
                    print("all done")




