#RAK experiments 2024 

import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from fit_data import *

import os
import pandas as pd
import pdb

#inVR we assumed 50Hz
#rak: 10-11Hz, driven by cmdvel from joycon ?

#we assume that all available ros2bags have first been merged into a single big csv file using:
#for file in  ~/docs/rak/gt_bag_files/*/*/*db3; do ros2 bag play $file; done
#ros2 topic echo /chicken_game_info --csv | tee  rak.csv


def loadPodcarDfsAll():
    dfs1 = loadPodcarDfsDay(1)
    dfs2 = loadPodcarDfsDay(2)
    dfs3 = loadPodcarDfsDay(3)
    dfs=[]
    for df in [dfs1, dfs2, dfs3]:
#    for df in [ dfs2 ]:
        dfs+=df
    return dfs


def loadPodcarDfsDay(day=2):  #day= 0,1,2,3
    dir_csv = "datasets/202406rakcarped/"
    fn = dir_csv+"rak%i.csv"%day   
    df = pd.read_csv(fn)   
    df.columns = ['t_s','t_ns','frameid','b_gameplayed','b_yield','box_car','box_ped','p_yield_car','p_yield_ped','x_car','y_car','z_car','rot_x_car','rot_y_car','rot_z_car','rot_w_car','x_ped','y_ped','z_ped','rot_x_ped','rot_y_ped','rot_z_ped','rot_w_ped']  #NB ped_y is -ve
#we are interested in x_car and -y_ped

	#NB these arrive in map coordinates, based on ROS coords from the car, so x is forward for the car. we later need to swap x and y.
    #flip axes to measure +ve dist to collision point
    df['x_car'] *= -1
    df['x_ped'] *= -1
    df['y_car'] *= -1
    df['y_ped'] *= -1
    #move coordinates so that 0,0 origin is at the collision point
    wx=0. ; wy=0.
    if day==0:    
        df['x_car'] *= -1  #flip x as this day approached from other direction?
        df['x_ped'] *= -1
        wx = 1.2 ;  wy = 2.5    #day 0 (CF test) correct
    elif day==1:
        wx = -5.0 ;  wy = 2.8    #day 1 
    else:
        wx = -4.0 ;  wy = 2.1    #day 2(mon) and day 3 (tue)
    df['x_car'] -= wx
    df['y_car'] -= wy
    df['x_ped'] -= wx
    df['y_ped'] -= wy

    df['d_car_squared'] = (df['x_car']**2 + df['y_car']**2)
    df['d_car'] = df['d_car_squared'].apply(lambda x: np.sqrt(x))
    df['d_ped_squared'] = (df['x_ped']**2 + df['y_ped']**2)
    df['d_ped'] = df['d_ped_squared'].apply(lambda x: np.sqrt(x))

    #reduce distances by half diameter of the agents, to report their front end location rather than center
    #df['d_car'] -= 0.50
    #df['d_ped'] -= 0.25

    df['subject'] = 0*df['x_car']  #init with zeros
    df['crossing'] = 0*df['x_car']

    #cut into crossings, and tag with subject and crossing IDs
    dfs = cutBigDfIntoPerGameDfs(df)
    #pdb.set_trace()
    if day==0:
        firstsubject=1
    if day==1:
        firstsubject=2
    if day==2:
        firstsubject=10
    if day==3:
        firstsubject=15
    #numbers taken from the original rosbag subdirs
    crossingbysubject=[11,    20, 20, 20, 24, 20, 20, 21, 20,     30, 31, 21, 20, 27   , 22, 10, 20, 20, 20, 20, 20, 20, 20, 22, 20, 20, 10]
#                      1      2   3   4   5   6   7   8   9       10  11  12  13  14     15  16  17  18  19  20  21  22  23  24  25  26  27

    subject=firstsubject
    crossing=1
    for df in dfs:
        df['subject'] = subject
        df['crossing'] = crossing
        crossing+=1
        if crossing>crossingbysubject[subject]:
            subject+=1
            crossing=1

    return dfs


def cutBigDfIntoPerGameDfs(df):
#    df['time_fsec'] = df['t_s'] + 0.000000001*df['t_ns']
#    df = df.sort_values('time_fsec')
#    pdb.set_trace()

    time_diff = df['t_s'].diff()
    jump_condition = time_diff > 1  #seconds
    split_dfs = [group for _, group in df.groupby(jump_condition.cumsum())]
    return split_dfs







#load a single game from a single-game df
def df2game(df):
    #inVR assumed 50Hz
    #rak: 10-11Hz, driven by cmdvel from joycon ?


    #to reproduce live recorded boxes
    df['box2_car'] = np.ceil(df['d_car']*2.5) - 2  ##the -2 makes it match the original CSV
    df['box2_ped'] = np.ceil(df['d_ped']*1)

    #to requantise higher res
    #df['box_car'] = np.ceil(df['d_car']*2.5* 5)    #was 2.5*5
    #df['box_ped'] = np.ceil(df['d_ped']*1.0* 5)    #was 1.0*2


    #TODO trying alternative local speed measures here, with rolling average of diffs
    df['s_car'] = np.pad(-np.diff(df['d_car']),(0,1))
    df['s_ped'] = np.pad(-np.diff(df['d_ped']),(0,1))
    df['s_car'] = df['s_car'].ewm(alpha=0.1  ).mean()  #EMWA smooth
    df['s_ped'] = df['s_ped'].ewm(alpha=0.1  ).mean()

    #pdb.set_trace()


    df = df.iloc[::10]   #downsample whole df
#    df.drop(0) #drop first datum as ped as stationary at start and needs to accelelrate up




    #pdb.set_trace()
    #TODO remove first 2 boxes of ped walk , assuming they accellerate up during them
##    df = df[df['box2_ped'] <= max(df['box2_ped'])-2 ]



    cs = array(df['box2_car'])    #carcells. resample to every 30th tick
    ps = array(df['box2_ped'])    #pedestrian cell

    dcs = np.pad(-np.diff(cs), (0,1)  )  #zeropad at end   speed, in boxes per sample
    dps = np.pad(-np.diff(ps), (0,1)  )  #zeropad at end


    #actions of car and ped
    if 0:
        acs = array(list(map( lambda d: 1 if abs(d)<1 else 2 , dcs )))   #actions for car, 1=SLOW 2=FAST    was <2
        aps = array(list(map( lambda d: 1 if abs(d)<1 else 2 , dps )))   #actions for ped
    else:
        #alt version using comparison to avg speed?   using box off diags? or avg numbers.
        cs_mean = df[['s_car']].mean()
        ps_mean = df[['s_ped']].mean()
       
#        pdb.set_trace()

        acs = array(list(map( lambda c: c>cs_mean-0.008   , df['s_car'] ))) +1   #convcert to 1,2 for slow,fast
        aps = array(list(map( lambda p: p>ps_mean+0.008   , df['s_ped'] ))) +1
    
        acs[0]=2 #assume init speed is fast (as speed not yet observed)
        aps[0]=2

    #pdb.set_trace()
    
    
    #reformat into std game list of list of lists
    game=[]
    for t in range(0, len(dcs)): #CHANGED FROM VR CODE
        turn = ( int(cs[t]), int(ps[t]), int(acs[t]), int(aps[t]) )   #car as Y player, ped as X
        game.append(turn)
    return game




def gamestats(games):
    ngames=0
    nturns=0
    npslow=0
    nplayed=0
    nplayedslow=0
    for game in games:
        ngames+=1
        for turn in game: 
            nturns+=1
            (cs,ps,dcs,dps) = turn
            if dps==1:
                npslow+=1
            if cs==ps:
                nplayed+=1
                if dps==1:
                    nplayedslow+=1
    print("ngames:%i nturns:%i npslow:%i nplayed:%i nplayedslow:%i"%(ngames,nturns,npslow,nplayed,nplayedslow))


if __name__ == "__main__":

    dfs = loadPodcarDfsAll()  #TODO could combine many now
    #plot(array(df_all['x_car']), array(df_all['y_car']), 'b.'); plot(array(df_all['x_ped']), array(df_all['y_ped']), 'r.'); show()


    games=[]
    g=0
    for df in dfs:

#        if df.shape[0]>50:   #ignore weird shorts
            game = df2game(df)
            games.append(game)
            g+=1

    gamestats(games)

#    U_crashes=[  -0.05, -0.1,  -0.5, -1.0, -2.,  -5., -7, -9, -10, -11, -12, -15,  -20, -50, -100, -1000, -10000, -100000, -1000000, -10000000]
    U_crashes=[ -1000000000000, -10000000, -1000, -10,  -1]
    
    if 1:
	    results = fit_data(games, NY=76, NX=76, U_crashes=U_crashes)
    
	    plot(U_crashes, results)
	    xlabel("U_crash (seconds)"); ylabel("loglik");
	    title("Podcar Experiment, all data")
	    show()


    (hist_slow, hist_fast) =  yieldhistogram(games)
    plot(hist_slow/(hist_slow+hist_fast)); show()

#maybe rak would like to do per-subject variation;  avg per-tme variation

