import pylab
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from matplotlib import animation
from operator import itemgetter


def func(x, y, lik):
    return lik[x, y]


def plots(results, title):
    
    # extract u_crash, u_time, likelihood from list of tuples as X, Y, Z
    UC = np.array([x[0] for x in results])   # u_crash
    UT = np.array([x[1] for x in results])   # u_time
    LL = np.array([x[2] for x in results])   #likelihood
    maxLL = max(LL)  # max of LL
       
    if 1:
        print("stem plot")
        fig = plt.figure(1)
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        for uc, ut, ll in zip(UC, UT, LL):        
            line=art3d.Line3D(*zip((uc, ut, 0), (uc, ut, ll)), marker='o', markevery=(1, 1))
            ax.add_line(line)
        ax.set_xlim3d( min(UC), max(UC) )
        ax.set_ylim3d( min(UT), max(UT) )
        ax.set_zlim3d( min(LL), 0)
        ax.set_title(title + ' -- Optimum: ' + str(round(maxLL, 3)))
        ax.set_xlabel("U_crash")
        ax.set_ylabel("u_time")
        ax.set_zlabel("LogLikelihood")
    
        #plt.savefig('stem_plot.png', bbox_inches='tight')
        plt.show()
        
    if 1:
        print("contour plot")
        
        plt.rcParams['xtick.direction'] = 'out'
        plt.rcParams['ytick.direction'] = 'out'
        
        print(UC)
        
        # generate u_crash, u_time as X,  Y arrays of integers 
        X = np.arange( min(UC), max(UC), 10)
        Y = np.arange( min(UT), max(UT), 4)
        X, Y = np.meshgrid(X, Y)
        
        print(min(UC))
        print(min(UT))
        print(max(UC))
        print(max(UT))
        print(X.shape)
        print(Y.shape)
        print("shape loglik: " + str(np.shape(LL)))
        
        #lik = np.array([i[2] for i in results]) # extract likelihood from list of tuples
        #print()
        #print(np.shape(lik))
        lik = np.reshape(LL, (X.shape[0], Y.shape[0]))  # give shape (X,Y) to loglik
        print(lik)
        print(np.shape(lik))
        
        Z = np.zeros((X.shape[0], Y.shape[0]))  # give shape (X,Y) to loglik
        for i in range(X.shape[0]):
            for j in range(Y.shape[0]):
                Z[i,j] = func(i, j, lik)  # fill Z from loglik
        
        # Create a simple contour plot with labels using default colors.  The
        # inline argument to clabel will control whether the labels are draw
        # over the line segments of the contour, removing the lines beneath
        # the label
        plt.figure()
        CS = plt.contour(X, Y, Z)
        plt.clabel(CS, inline=1, fontsize=10)
        plt.title(title + " -- Optimum: " + str(round(maxLL,3)))         
        plt.xlabel("U_crash")
        plt.ylabel("U_time")
        #plt.xlim(-xlim, 0)
        #plt.ylim(0, ylim)
        #plt.zlim(-zlim, 0)
        plt.show()
        
      
    if 1:
        plt.figure(3)
        
         
        print("Colormap plot")
        
        # generate u_crash, u_time as X,  Y arrays of integers 
        X = np.arange( min(UC), max(UC), 10)
        Y = np.arange( min(UT), max(UT), 4)
        X, Y = np.meshgrid(X, Y)
       
        print("X shape" + str(X.shape))
        print("Y shape" + str(Y.shape))
        #X,Y = np.meshgrid(X,Y)
        
        #lik = LL # extract likelihood from list of tuples
        lik = np.reshape(LL, (X.shape[0], Y.shape[1]))  # give shape (X,Y) to loglik
        print(lik)
        print(np.shape(lik))
        
        Z = np.zeros((X.shape[0], Y.shape[1]))  # create empty array with X shape
        print("Shape z: " + str(np.shape(Z)))
        for i in range(X.shape[0]):
            for j in range(Y.shape[1]):
                Z[i,j] = func(i, j, lik)  # fill in Z with loglik values
    
        plt.title(title + " -- Optimum: " + str(round(maxLL,3)))
        plt.xlabel("U_crash")
        plt.ylabel("U_time")
    
        # plot the calculated function values
        plt.pcolor(X,Y,Z)
        # and a color bar to show the correspondence between function value and color
        plt.colorbar()
        plt.show() 
        
        
        
if __name__ == "__main__":
   plots(results, "title") 
    
    
    
    
