#HFIV2020 book chapter experiments

import GPy, numpy as np
import matplotlib.pyplot as plt

import load_data_continuous
from fit_data import *
import gaussianProcessAnalysis

if __name__ == "__main__":
    ds = 0.1  # 0.1m
    dt = 0.09 # time step = 0.09s
    vp = 1.0  # assumes pedestrian average speed = 1.0 m/s
    dir_data = "datasets/HFIV2020bookchapterContinuousLidar/"
    games = load_data_continuous.load_data(dir_data, ds, dt, vp)
    print(games)

    U_crashes=[-0.05, -0.1, -.2, -.3, -.4, -0.5, -0.75, -1.0, -1.5, -2., -3., -4. -5, -10, -20, -50, -100, -1000]
    results = fit_data(games, NY=60, NX=60, U_crashes=U_crashes)

    plot(U_crashes, results)
    xlabel("U_crash (seconds)"); ylabel("loglik"); title("Lidar Continuous game")
    #gaussianProcessAnalysis.gplot(results, "Lidar Continuous game") 

   
