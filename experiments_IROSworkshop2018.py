#Experiments from IROS workshop 2018 paper
#which has physical humans moving their bodies in the turn by turn squared game

import GPy, numpy as np
import matplotlib.pyplot as plt

from fit_data import *
import gaussianProcessAnalysis



def lidarBoardGame():
    filename = "datasets/IROSworkshopLidarBoardGames2018/track_file_data.csv"
    games = load_data(filename)
    U_crashes=[-0.05, -0.1, -.2, -.3, -.4, -0.5, -0.75, -1.0, -1.5, -2., -3., -4. -5, -10, -20, -50, -100, -1000]
    results = fit_data(games, NY=20, NX=20, U_crashes=U_crashes)
    plot(U_crashes, results)
    xlabel("U_crash (seconds)"); ylabel("loglik"); title("Lidar Board game")
    #gaussianProcessAnalysis.gplot(results, "Natural game") 

if __name__ == "__main__":
    lidarBoardGame() 
    
