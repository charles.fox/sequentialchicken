#experiments from TRF VR experiment1 

import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from fit_data import *
import gaussianProcessAnalysis

import os
import pandas as pd



###TODO need tro mod fitdata for VR so only fits to PED not to car.


def firstIndexInListWithBooleanProperty(xs, f):   #f:x->bool
    n=len(xs)
    for i in range(0,n):
        if f(xs[i]):
            return i
    return n

def vals2deltas(xs):
    n=len(xs)
    deltas = zeros(n)
    for i in range(0,n-1):
        deltas[i] = xs[i]=xs[i+1]   #this leaves zero for the final speed
    return deltas

def quantiseSpeeds(xs):
    return array(list(map( lambda x: 1 if x<2 else 2 , xs )))

#load a single game from csv
def VRCSV2game(fn_csv='datasets/TRF_VR_2021/Experiment_1/Experiments/2019-04-25 18_39_25.csv'):
    df = pd.read_csv(fn_csv)    

    cs = array(df[' car_pos_cell'])[::30]    #carcells. resample to every 30th tick
    ps = array(df[' ped_pos_cell'])[::30]    #pededestrian cells.

    #crop to time data when someone arrives
    t_car_arrives = firstIndexInListWithBooleanProperty(cs, lambda x: x<2)
    t_ped_arrives = firstIndexInListWithBooleanProperty(ps, lambda x: x<2)
    t_someone_arrives = min(t_car_arrives, t_ped_arrives)
    cs = cs[0:t_someone_arrives]
    ps = ps[0:t_someone_arrives]

    #NB these often have steps bigger than 2 or less than 1.
    #lets classify all steps of 2 or more as 2 = FAST = NOYIELD
    #and all steps of 1 or less as 1 = SLOW = YIELD)
    dcs = quantiseSpeeds(vals2deltas(cs))
    dps = quantiseSpeeds(vals2deltas(ps))
    
    #TODO now reformat into game list of list of lists...
    game=[]
    for t in range(0, t_someone_arrives):
        #create 4tuple turn state
        turn = ( int(cs[t]), int(ps[t]), int(dcs[t]), int(dps[t]) )   #car as Y player, ped as X
        game.append(turn)
    return game

#load all games from VR CSV (for Experiment 1 only)
def VRCSV2games():
    dir_csv = "datasets/TRF_VR_2021/Experiment_1/Experiments/"
    games=[]
    for fn in os.listdir(dir_csv):
        fn_full=dir_csv+fn
        game = VRCSV2game(fn_full)
        games.append(game)
    return games



def analyze2D():
    games = VRCSV2games()
    results2d = fit_data2D(games,  U_crashes=range(-100,10,10), U_times=range(0,400,80))
    gaussianProcessAnalysis.gplot(results2d, "Natural game") 


def analyze1D():
    games = VRCSV2games()
    U_crashes=[-0.05, -0.1, -.2, -.3, -.4, -0.5, -0.75, -1.0, -1.5, -2., -3., -4. -5, -10, -20, -50, -100, -1000]
    results = fit_data(games, NY=45, NX=45, U_crashes=U_crashes)
    plot(U_crashes, results)
    xlabel("U_crash (seconds)"); ylabel("loglik");
    title("VR Experiment 1")


if __name__ == "__main__":
#    analyze2D()
    analyze1D()

