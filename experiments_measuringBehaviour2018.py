#experiments from Measuring Behaviour 2018 paper

import GPy, numpy as np
import matplotlib.pyplot as plt

from pylab import *

from fit_data import *
import gaussianProcessAnalysis



def naturalGame():
    filename = "datasets/measuringBehaviourBoardGames2018/boardgame_natural.csv"
    games = load_data(filename)
    U_crashes=[-0.05, -0.1, -.2, -.3, -.4, -0.5, -0.75, -1.0, -1.5, -2., -3., -4. -5, -10, -20, -50, -100, -1000]
    results = fit_data(games, NY=20, NX=20, U_crashes=U_crashes)
    plot(U_crashes, results)
    xlabel("U_crash (seconds)"); ylabel("loglik"); 
    #gaussianProcessAnalysis.gplot(results, "Natural game")     #REMOVED as we only compute for Utime=1 now

def chocolateGame():
    filename = "datasets/measuringBehaviourBoardGames2018/boardgame_chocolate.csv"
    games = load_data(filename)
    U_crashes=[-0.05, -0.1, -.2, -.3, -.4, -0.5, -0.75, -1.0, -1.5, -2., -3., -4. -5, -10, -20, -50, -100, -1000]
    results = fit_data(games, NY=20, NX=20, U_crashes=U_crashes)
    plot(U_crashes, results)
    xlabel("U_crash (seconds)"); ylabel("loglik");
    #gaussianProcessAnalysis.gplot(results, "Natural game") 


#as in the paper, 2d gaussian process
def naturalGameOld2D():
    filename = "datasets/measuringBehaviourBoardGames2018/boardgame_natural.csv"
    games = load_data(filename)
    results2d = fit_data2D(games)
    gaussianProcessAnalysis.gplot(results2d, "Natural game")   



if __name__ == "__main__":
    #naturalGameOld2D()   #to reproduce whats in the paper, 2d GP analysis

    #new analysis in 1D using time as currency
    naturalGame()
    figure()
    chocolateGame()
    title("blue=natural, green-chocolate")
