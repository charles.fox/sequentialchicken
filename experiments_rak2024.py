#RAK experiments 2024 

import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from fit_data import *

import os, math
import pandas as pd
import pdb
import pickle

pd.set_option('display.max_rows', None)   #display all df rows

dir_csv = "datasets/202406rakcarped/"

def getTriples():
    triples=[]
    for file in sorted(os.listdir(dir_csv)):
        fn = os.fsdecode(file)
        bits = re.split("_", fn)
        day = int(bits[0][3:])
        subject = int(bits[1][3:])
        crossing = int(bits[3][:-4])
        triple = [day,subject,crossing]
        triples+=[triple]
    return triples


def loadPodcarDfsAll():
    dfs=[]
    if 1:
        triples = getTriples()   #day,sub,crossing triples of available data
        for triple in triples:
            (day,subject,crossing) = triple
            if not day==0:
                print(triple)
                df = dfOneCrossing(day,subject,crossing)
                dfs.append(df)
        with open("dfs.pickle", "wb") as f:
            pickle.dump(dfs, f)
    else:
        print("LOADING CACHED DFS")
        with open("dfs.pickle", "rb") as f:
            dfs= pickle.load(f)
    return dfs


def dfOneCrossing(day, subject, crossing):  #day= 0,1,2,3
    fn = dir_csv+"day%i_Sub%i_cross_%i.csv"%(day,subject,crossing)
    df = pd.read_csv(fn)   
    df.columns = ['time','frameid','b_gameplayed','b_yield','box_car','box_ped','p_yield_car','p_yield_ped','x_car','y_car','z_car','rot_x_car','rot_y_car','rot_z_car','rot_w_car','x_ped','y_ped','z_ped','rot_x_ped','rot_y_ped','rot_z_ped','rot_w_ped']  #NB ped_y is -ve
    df=df.drop('rot_x_ped', axis=1)  #delete unused data to make easier for screen display
    df=df.drop('rot_y_ped', axis=1)
    df=df.drop('rot_z_ped', axis=1)
    df=df.drop('rot_w_ped', axis=1)
    df=df.drop('rot_x_car', axis=1)  #delete unused data to make easier for screen display
    df=df.drop('rot_y_car', axis=1)
    df=df.drop('rot_z_car', axis=1)
    df=df.drop('rot_w_car', axis=1)
    df=df.drop('z_car', axis=1)
    df=df.drop('z_ped', axis=1)

    #we are interested in x_car and -y_ped
	#NB these arrive in map coordinates, based on ROS coords from the car, so x is forward for the car. we later need to swap x and y.
    #flip axes to measure +ve dist to collision point
    df['x_car'] *= -1
    df['x_ped'] *= -1
    df['y_car'] *= -1
    df['y_ped'] *= -1
    #move coordinates so that 0,0 origin is at the collision point
    wx=0. ; wy=0.
    if day==0:    
        df['x_car'] *= -1  #flip x as this day approached from other direction?
        df['x_ped'] *= -1
        wx = 1.2 ;  wy = 2.5    #day 0 (CF test) correct
    elif day==1:
        wx = -5.0 ;  wy = 2.8    #day 1 
    else:
        wx = -4.0 ;  wy = 2.1    #day 2(mon) and day 3 (tue)
    df['x_car'] -= wx
    df['y_car'] -= wy
    df['x_ped'] -= wx
    df['y_ped'] -= wy

    df['d_car_squared'] = (df['x_car']**2 + df['y_car']**2)
    df['d_car'] = df['d_car_squared'].apply(lambda x: np.sqrt(x))
    df['d_ped_squared'] = (df['x_ped']**2 + df['y_ped']**2)
    df['d_ped'] = df['d_ped_squared'].apply(lambda x: np.sqrt(x))

    #reduce distances by half diameter of the agents, to report their front end location rather than center
    df['d_car'] -= 1.3
    df['d_ped'] -= 0.25

    df['subject'] = subject
    df['crossing'] = crossing

    #to reproduce and check live recorded boxes
#    df['box2_car'] = np.round(df['d_car']*2.5) - 2  ##the -2 makes it match the original CSV
#    df['box2_ped'] = np.round(df['d_ped']*1)

    #to requantise higher res
    Q=5
    df['box2_car'] = np.round((df['d_car']*2.5)* Q)    #was 2.5*5
    df['box2_ped'] = np.round(df['d_ped']*1.0* Q)    #was 1.0*2

    #removing ped accelleration and bad detections
#    df = df[df['box2_ped'] < Q*10]    #the ped start region has 8 boxes so anything furether than 8 is a bad detection or other glitch, so remove
    if df.shape[0]>0:
        startboxped = df['box2_ped'].iloc[0]
        df = df[df['box2_ped'] <= startboxped-(3*Q)]    #assume it takes 2Q boxes for peds to accelelrate up to full speed

    return df




#load a single game from a single-game df
def df2game(df):
    #in VR we assumed 50Hz data
    #rak: recorded at 10-11Hz, driven by cmdvel from joycon -- downsample to 1Hz

    #resample to 1s equal times (ros2 is irregular)
    df['time'] = pd.to_datetime(df['time'], unit='s')
    df = df.set_index('time')
    df = df.resample('1s').ffill()
    df = df.iloc[1:]   #rm nan first row

    #rolling average of diffs to define local speed
    df['s_car'] = np.pad(-np.diff(df['d_car']),(0,1))
    df['s_ped'] = np.pad(-np.diff(df['d_ped']),(0,1))

    cs = array(df['box2_car'])    #carcells. resample to every 30th tick
    ps = array(df['box2_ped'])    #pedestrian cell

    dcs = np.pad(-np.diff(cs), (0,1)  )  #zeropad at end   speed, in boxes per sample
    dps = np.pad(-np.diff(ps), (0,1)  )  #zeropad at end


    #find the final box seen containing ped.   cut data so we only see one decision make at it (because if they yield, they can then stand there yielding for ages waiting for car to pass)
    b_removedpad = False
    if (True):
        idx = np.where(ps==min(ps))[0][0] 
        if idx<len(ps)-1:
            b_removedpad=True
        ps = ps[:idx+1]
        cs = cs[:idx+1]
        dps = dps[:idx+1]
        dcs = dps[:idx+1]


    #actions of car and ped
    if 1:
        acs = array(list(map( lambda d: 1 if abs(d)<2 else 2 , dcs )))   #actions for car, 1=SLOW 2=FAST    was <2
        aps = array(list(map( lambda d: 1 if abs(d)<2 else 2 , dps )))   #actions for ped
    else:
        #version using comparison to avg speed.      (could also try using box off diags here?)
        cs_mean = df[['s_car']].mean()
        ps_mean = df[['s_ped']].mean()
        acs = array(list(map( lambda c: c>cs_mean+0.001   , df['s_car'] ))) +1   #convcert to 1,2 for slow,fast   mean+0.008
        aps = array(list(map( lambda p: p>ps_mean+0.001   , df['s_ped'] ))) +1
    
    #reformat into std game list of list of lists
    game=[]
    Nmax= len(dcs)-1
    if b_removedpad:
        Nmax=len(dcs)
    for t in range(0, Nmax):   #-1 len so we dont count the fake padded 0 speed in the last box
        turn = ( int(cs[t]), int(ps[t]), int(acs[t]), int(aps[t]) )   #car as Y player, ped as X
        game.append(turn)
    return game




def gamestats(games):
    ngames=0
    nturns=0
    npslow=0
    nplayed=0
    nplayedslow=0

    ncarwins=0
    npedwins=0
    ndraws=0
    for game in games:
        ngames+=1
        b_chickenplayed=False
        for turn in game: 
            nturns+=1
            (cs,ps,dcs,dps) = turn
            if dps==1:
                npslow+=1
            if cs==ps:
                nplayed+=1
                b_chickenplayed=True
                if dps==1:
                    nplayedslow+=1
        #who won? only count games where a game state existed
        if b_chickenplayed:
            if cs<ps:
                ncarwins+=1
            if ps<cs:
                npedwins+=1
            if cs==ps:
                ndraws+=1
    print("ngames:%i nturns:%i npslow:%i nplayed:%i nplayedslow:%i"%(ngames,nturns,npslow,nplayed,nplayedslow))
    print("ncarwins:%i npedwins:%i ndraws:%i"%(ncarwins,npedwins,ndraws))



#probability of an event if it has n attempts each with prob p in a poisson series
def poissonsum(p, n):
    if n<1:
        return 0
    else:
        p_total = 0
        for i in range (0,n):
            p_total += (1-p)**i * p
        return p_total

#testing requantise model to group n boxes into 1
def reqmodel(S, n):
    d = diag(S[:,:,0])
    i=0
    out=[]
    while i<len(d)+n:
        pd = sum(d[i:i+n])/ n     #TODO not sum, poisson or fuse here
        out.append(pd)
        i+=n
    return out


def pdf(dfs, i):
    df=dfs[i]
    plot(array(df['x_car']), array(df['y_car']), 'b.'); plot(array(df['x_ped']), array(df['y_ped']), 'r.'); show()



#beta distribution stats
def updateBeta(alpha_prior, beta_prior, N, k):
    alpha_post = alpha_prior + k
    beta_post = beta_prior + N - k
    return (alpha_post, beta_post)
def getBeataMeanStdev(alpha, beta):
    mean = alpha/(alpha+beta)
    var = (alpha*beta)/( (alpha+beta)**2 * (alpha+beta+ 1)  )
    stdev = math.sqrt(var)
    return(mean, stdev)
def getBetaConfIntervals(xs, ys):   #one std for each x/(x+y) in series
    ps = np.zeros(len(xs))
    sigmas = np.zeros(len(xs))
    for i in range(0, len(xs)):
        k = xs[i]
        N=ys[i]+xs[i]
        (a,b) = updateBeta(1,1, N,k)
        (mean,stdev)=getBeataMeanStdev(a,b)
        ps[i]=mean
        sigmas[i]=stdev
    return (ps,sigmas)
def getFrequentistConfIntervals(xs, ys):   #one std for each x/(x+y) in series
    ps = np.zeros(len(xs))
    sigmas = np.zeros(len(xs))
    for i in range(0, len(xs)):
        k = xs[i]
        N=ys[i]+xs[i]
        p_hat = k/N
        ps[i]=p_hat
        sigmas[i]=  math.sqrt( p_hat*(1-p_hat) / N   )
    return (ps,sigmas)



def cumprobs(ps):   #r_data
    ps = list(reversed(ps))

    cumprobs = np.zeros(len(ps))
    p_reach_is = np.zeros(len(ps))
    p_reach_i=1.0

    for i in range(0, len(ps)):
        p_reach_i_then_yield = p_reach_i * ps[i]
        p_reach_i = p_reach_i * (1-ps[i])
        cumprobs[i] = p_reach_i_then_yield
        p_reach_is[i]=1-p_reach_i
    cumprobs=list(reversed(cumprobs))
    p_reach_is=list(reversed(p_reach_is))
    return (cumprobs, p_reach_is)


if __name__ == "__main__":

    dfs = loadPodcarDfsAll()

    games=[]
    g=0
    for df in dfs:
        if df.shape[0]>50:   #ignore weird shorts
            game = df2game(df)
            games.append(game)
            g+=1

    gamestats(games)
    (hist_slow, hist_fast) =  yieldhistogram(games)
    hist_slow=hist_slow[0:22]
    hist_fast=hist_fast[0:22]
    hist_slow[0:2]=np.nan
    hist_fast[0:2]=np.nan


    r_data = hist_slow/(hist_slow+hist_fast)          #results, from the data
    r_data[0:2] = np.nan


    #loglik analysis
    if 0:
        U_crashes=[  -0.05, -0.1,  -0.5, -1.0, -2.,  -5., -7, -9, -10, -11, -12, -15,  -20, -50, -100, -1000, -10000, -100000, -1000000, -10000000]
        U_crashes=[ -3,  -1000000000000, -10000000, -1000, -100,  -10,  -1]
        results = fit_data(games, NY=76, NX=76, U_crashes=U_crashes)
    
        plot(U_crashes, results)
        xlabel("U_crash (seconds)"); ylabel("loglik");
        title("Podcar Experiment, all data")
        show()



    #yield curve
    if 1:
        ds = np.array(range(0, len(r_data)))/5.0

        (mus_beta, sigs_beta) = getBetaConfIntervals(hist_slow, hist_fast)
        (mus, sigs) = getFrequentistConfIntervals(hist_slow, hist_fast)


        fig, ax = plt.subplots()

        plot(ds, r_data, 'k-')
        ax.fill_between(ds, mus_beta+sigs_beta, mus_beta-sigs_beta, alpha=0.2)
        plot(ds, mus, 'k-')
#        show()


        import sequentialChicken
        (V,S) = sequentialChicken.solveGame(U_crash_Y=-3, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
    #    plot(r_model1, 'k:')
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k--')

        xlabel("Pedestrian distance to collision / m")
        ylabel("P(pedestrian yield|pedestrian distance)")


        (V,S) = sequentialChicken.solveGame(U_crash_Y=-100000, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')


        (V,S) = sequentialChicken.solveGame(U_crash_Y=-10000, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')

        (V,S) = sequentialChicken.solveGame(U_crash_Y=-1000, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')

        (V,S) = sequentialChicken.solveGame(U_crash_Y=-100, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')

        (V,S) = sequentialChicken.solveGame(U_crash_Y=-10, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')

        (V,S) = sequentialChicken.solveGame(U_crash_Y=-2, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan
        ds = np.array(range(0, len(r_model2)))/5.0
        plot(ds, r_model2, 'k:')


    if 1:
        figure()

        #compute and graph cumulative probabilities of a ped yield occuring anytime up to each distance
        r_data_edit = r_data.copy()[0:22]
        r_data_edit[0] = 0.5   #no data for the two crash zones, fill in with maxent 0.5
        r_data_edit[1] = 0.5   #no data for the two crash zones, fill in with maxent 0.5
        rs = r_data_edit


        import sequentialChicken
        (V,S) = sequentialChicken.solveGame(U_crash_Y=-3, U_crash_X=-3, U_time=1., NY=22, NX=22); 
        r_model1 = diag(S[:,:,0]).copy()      #result, model
        r_model2 = diag(S[:,:,1]).copy()
        r_model1[0:2] = np.nan    #dont predict anything in the crash zone
        r_model2[0:2] = np.nan

        ms = r_model2[0:24]

        (cp,ri) = cumprobs(rs)
        (cpm,rim) = cumprobs(ms)

        clf()
        ds = np.array(range(0, len(ri)))/5.0
        plot(ds, ri,'k')
        ds = np.array(range(0, len(rim)))/5.0
        rim[0]=np.nan
        rim[1]=np.nan
        plot(ds, rim,'k--')
        xlabel("Pedestrian distance to collision / m")
        ylabel("cumulative P(ped. yield up to ped. distance)")

    show()

