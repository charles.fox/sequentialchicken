
import sequentialChicken
import random
from pylab import *
SEED=21


def experiment_game_sym():
    (V,S) = sequentialChicken.solveGame(U_crash_Y=-20, U_crash_X=-20, U_time=1, NY=20, NX=20)

    print(V[:,:,0])
    print(V[:,:,1])
    print("S")
    print(S[:,:,0])
    print(S[:,:,1])

    figure(1)
    clf()
    imshow(V[:,:,0], interpolation='none', cmap='gray')
    xlabel("x, player X location / meters")
    ylabel("y, player Y location / meters")
    title("Player Y Values, both to play")
    colorbar();
    savefig('V.png', bbox_inches='tight')

    figure(5)
    clf()
    imshow(V[:,:,1], interpolation='none', cmap='gray')
    xlabel("x, player X location / meters")
    ylabel("y, player Y location / meters")
    title("Player X Values, both to play")
    colorbar();
#savefig('VX.png', bbox_inches='tight')


    ystart=10
    xstart=10
    figure(2)
    clf()
    sequentialChicken.sim(S, ystart, xstart, SEED)
    savefig('sim.png', bbox_inches='tight')

    (P, p_crash) = sequentialChicken.computeStateProbs(S, ystart, xstart)
    figure(4)
    clf()
    imshow(P, interpolation='none', cmap='gray'); 
    colorbar();
    s='State probabilities (P_crash=%0.4f)'%p_crash
    title(s)
    xlabel("x, player X location / meters")
    ylabel("y, player Y location / meters")
    savefig('P.png', bbox_inches='tight')

    figure(7)
    clf()
    imshow(S[:,:,0], interpolation='none', cmap='gray')
    xlabel("x, player X location / meters")
    ylabel("y, player Y location / meters")
    title('Player Y strategy, P(yield|y,x)')
    colorbar()
    savefig('S.png', bbox_inches='tight')

    figure(6)
    clf()
    imshow(S[:,:,1], interpolation='none', cmap='gray')
    xlabel("x, player X location / meters")
    ylabel("y, player Y location / meters")
    title('Player X strategy, P(yield|y,x)')
    colorbar()
#savefig('SU.png', bbox_inches='tight')

    show()

def experiment_game_strengths():
    p_crashes=[]
    #u_crashes = [   -1,  -10, -50,  -100, -250,  -500, -750,  -1000, -2500, -5000, -10000]
    u_crashes = [   -.01, -.1, -1, -1.5, -2, -2.5, -3, -4, -5, -6, -7, -8, -9, -10]
    for u_crash in u_crashes:
        (V,S) = sequentialChicken.solveGame(u_crash, u_crash, NY=20, NX=20)
        if 0:   
            print(V[:,:,0])
            print(V[:,:,1])
            print("S")
            print(S[:,:,0])
            print(S[:,:,1])
        if 0:
            figure(1)
            imshow(V[:,:,0], interpolation='none', cmap='gray')
            ylabel("Y location (meters)")
            xlabel("X location (meters)")
            title("Value, both to play")
            savefig('V.png', bbox_inches='tight')
        if 1:
            ystart=4
            xstart=4
            (P, p_crash) = sequentialChicken.computeStateProbs(S, ystart, xstart)
            p_crashes.append(p_crash)
        if 0:
            figure(2)
            sim(S, mstart, ustart, SEED)
            savefig('sim.png', bbox_inches='tight')
        if 0:
            figure(4)
            imshow(P, interpolation='none', cmap='gray');
            colorbar();
            s='P_crash=%0.4f'%p_crash
            title(s)
            xlabel("X, your location (meters)")
            ylabel("Y, my location (meters)")
            savefig('P.png', bbox_inches='tight')
        if 0:
            figure(4)
            imshow(S[:,:,0], interpolation='none', cmap='gray')
            xlabel("X, your location (meters)")
            ylabel("Y, my location (meters)")
            title('Player Y strategy, P(yield|Y,X)')
            colorbar()
            savefig('S.png', bbox_inches='tight')

    plot(u_crashes, p_crashes)
    xlabel('V(collision)')
    ylabel('P(collision)')
    title('Effect of varying shared collision utility')
    show()


def experiment_game_sym_vary_crash_val():
    p_crashes=[]
    v_crashes = [  -10, -50,  -100, -250,  -500, -750,  -1000, -2500, -5000, -10000]
    for v_crash in v_crashes:

        (V,S) = sequentialChicken.solveGame(v_crash, v_crash, NY=20, NX=20)

        if 0:
            print(V[:,:,0])
            print(V[:,:,1])
            print("S")
            print(S[:,:,0])
            print(S[:,:,1])
        
        if 0:
            figure(1)
            imshow(V[:,:,0], interpolation='none', cmap='gray')
            xlabel("u, your location (meters)")
            ylabel("m, my location (meters)")
            title("Value, both to play")
            savefig('V.png', bbox_inches='tight')


        if 1:
            mstart=12
            ustart=12
            (P, p_crash) = sequentialChicken.computeStateProbs(S, mstart, ustart)
            p_crashes.append(p_crash)
        if 0:
            figure(2)
            sequentialChicken.sim(S, mstart, ustart, SEED)
            savefig('sim.png', bbox_inches='tight')
        if 0:
            figure(4)
            imshow(P, interpolation='none', cmap='gray');
            colorbar();
            s='P_crash=%0.4f'%p_crash
            title(s)
            xlabel("u, your location (meters)")
            ylabel("m, my location (meters)")
            savefig('P.png', bbox_inches='tight')

        if 0:
            figure(4)
            imshow(S[:,:,0], interpolation='none', cmap='gray')
            xlabel("u, your location (meters)")
            ylabel("m, my location (meters)")
            title('Player M strategy, P(yield|m,u)')
            colorbar()
            savefig('S.png', bbox_inches='tight')

    plot(v_crashes, p_crashes)
    xlabel('V(collision)')
    ylabel('P(collision)')
    title('Effect of varying shared collision utility')
    show()

def experiment_game_asym_biggap():
    (V,S) = sequentialChicken.solveGame(U_crash_Y=-20, U_crash_X=-20, U_time=1, NY=20, NX=20)

    if 1:
        print(V[:,:,0])
        print(V[:,:,1])
        print("S")
        print(S[:,:,0])
        print(S[:,:,1])

    if 1:
        figure(1)
        clf()
        imshow(V[:,:,0], interpolation='none', cmap='gray')
        xlabel("x, player X location / meters")
        ylabel("y, player Y location / meters")
        title("Player Y Values, both to play")
        colorbar();
        savefig('V.png', bbox_inches='tight')

    if 1:
        figure(5)
        clf()
        imshow(V[:,:,1], interpolation='none', cmap='gray')
        xlabel("x, player X location / meters")
        ylabel("y, player Y location / meters")
        title("Player X Values, both to play")
        colorbar();
#savefig('VX.png', bbox_inches='tight')

    if 1:
        ystart=12
        xstart=10
        figure(2)
        clf()
        sequentialChicken.sim(S, ystart, xstart, SEED)
        savefig('sim.png', bbox_inches='tight')

    if 1:
        (P, p_crash) = sequentialChicken.computeStateProbs(S, ystart, xstart)
        figure(4)
        clf()
        imshow(P, interpolation='none', cmap='gray');
        colorbar();
        s='State probabilities (P_crash=%0.4f)'%p_crash
        title(s)
        xlabel("x, player X location / meters")
        ylabel("y, player Y location / meters")
        savefig('P.png', bbox_inches='tight')

    if 1:
        figure(7)
        clf()
        imshow(S[:,:,0], interpolation='none', cmap='gray')
        xlabel("x, player X location / meters")
        ylabel("y, player Y location / meters")
        title('Player Y strategy, P(yield|y,x)')
        colorbar()
        savefig('S.png', bbox_inches='tight')

    if 1:
        figure(6)
        clf()
        imshow(S[:,:,1], interpolation='none', cmap='gray')
        xlabel("x, player X location / meters")
        ylabel("y, player Y location / meters")
        title('Player X strategy, P(yield|y,x)')
        colorbar()
#savefig('SU.png', bbox_inches='tight')

        show()


if __name__ == "__main__":
    experiment_game_sym()
    #experiment_game_strengths()    
    #experiment_game_sym_vary_crash_val()
    #experiment_game_asym_biggap()
