import numpy as np
import GPy
import pandas  as pd
from operator import itemgetter
import sequentialChicken 
from cfplot import *
from load_data import *

#null chance model data logliklihood for a single (discrete) game, fittin oly X (ped) player
def loglik4game_nullchance_oneplayer(game, p_yield):
    loglik_game = 0.
    for turn in game:
        (y,x, a_y,a_x) = turn               #usually Y=car, X=ped
        lik_X_yield = p_yield       
        if a_x==1:
            lik_X = lik_X_yield
        else:
            lik_X = 1-lik_X_yield
        lik_turn = lik_X
        loglik_turn = np.log(lik_turn)
        loglik_game+= loglik_turn
    return loglik_game 


#seq chick data logliklihood for a single (discrete) game, fittin oly X (ped) player
def loglik4game_seqchicken_oneplayer(game, S):
    loglik_game = 0.
    for turn in game:
        (y,x, a_y,a_x) = turn               #usually Y=car, X=pe
        if y!=x:        #only consider interesting game states
            lik_X = 1   #1 has no effect on product of liks
        if x<3:
            lik_X = 1 # ignore crash states -- actions dont matter here
        if x>20:
            lik_X = 1 #ignore any junk outside max range
        else:
            lik_X_yield = S[y+1, x+1, 1]        #1 here means the ped (0 is car)    +1 as we used both 0 and 1 for crash (really -1 and 0)
            if a_x==1:
                lik_X = lik_X_yield 
            else:
                lik_X = 1-lik_X_yield  
        loglik_turn = np.log(lik_X)
        loglik_game+= loglik_turn
    return loglik_game 


#seq chick data logliklihood for a single (discrete) game, fitting both players
def loglik4game_seqchicken_bothplayers(game, S):
    loglik_game = 0.
    for turn in game:
        (y,x, a_y,a_x) = turn               #usually Y=car, X=ped
        lik_Y_yield = S[y, x, 0]
        lik_X_yield = S[y, x, 1]       
        if a_y==1:
            lik_Y = lik_Y_yield  
        else:
            lik_Y = 1-lik_Y_yield
        if a_x==1:
            lik_X = lik_X_yield
        else:
            lik_X = 1-lik_X_yield
        lik_turn = lik_X * lik_Y
        loglik_turn = np.log(lik_turn)
        loglik_game+= loglik_turn
    return loglik_game 



#202406 include probs of using rational seqchicken and a null chance model
def add_noise_to_strategy(S, p_irrational, p_yieldbychance):   #was all +.11
    #2step: filip coin with p_err to choose rational or not. if not rational, yield with a second prob.
    (R,C, nplayers) = S.shape
    for player in range(0,nplayers):
        for r in range(0,R):
            for c in range(0,C):
                p0_yield = S[r,c,player] 
                p_yield = (1-p_irrational)*p0_yield  + p_irrational*p_yieldbychance   
                S[r,c,player]= p_yield
    return S


def print_stuff(V,S,U_crash,U_time): 
    print("U_crash")
    print(U_crash)
    print("U_time")
    print(U_time)
            


#new 1D version, using units of time as currency. brute force search over single U_crash param for data logliklihood
def fit_data(games, NY=20, NX=20, U_crashes=range(-100, 10,10), model=loglik4game_seqchicken_oneplayer):   
    U_time = 1.0        #20200706 assume crash utility mesaured in units of time
    results = []        # log_likelihood values for all U_crash and U_time as a list of tuples (U_crash, U_time, likelihood)
    for U_crash in U_crashes:
            U_crash = U_crash+0.0
            (V,S0) = sequentialChicken.solveGame(U_crash, U_crash, U_time, NY, NX)
            #print_stuff(V,S0,U_crash, U_time)
            S = add_noise_to_strategy(S0, p_irrational=0.99, p_yieldbychance=0.43)  #0.5587229)  #TODO search over params
#            pdb.set_trace()
            loglik_all_games = 0.
            for game in games:
                loglik_game = model(game, S)
                loglik_all_games += loglik_game
            print("U_crash: %f , U_time:%f,  logP(all game data|U_crash, U_time):%f"%(U_crash,U_time,loglik_all_games))
            results.append(loglik_all_games)    #results is a list of logliks for U_crashes 20200706
    return results


#version used in MeasuringBehavior and other empirical papers, using 2D coords, for GP
def fit_data2D(games, U_crashes=range(-100,10,10), U_times=range(0,60,10)):
	results = []		# log_likelihood values for all U_crash and U_time as a list of tuples (U_crash, U_time, likelihood)
	for U_crash in U_crashes:
		for U_time in U_times: 
			U_crash = U_crash+0.0
			U_time = U_time +0.0
			(V,S0) = sequentialChicken.solveGame(U_crash, U_crash, U_time, NY=45, NX=45)
			print_stuff(V,S0,U_crash, U_time)

			S = add_noise_to_strategy(S0)
            #pdb.set_trace()
			loglik_all_games = 0.
			for game in games:
				loglik_game = loglik4game(game, S)
				loglik_all_games += loglik_game
			results.append((U_crash, U_time, loglik_all_games))
			print("all game log_lik" + str(loglik_all_games))
	return results




#different analysis, looking at yield profiles

def yieldhistogram(games):
    hist_slow = np.zeros(70)
    hist_fast = np.zeros(70)
    for game in games:
        for turn in game:
            (y,x, a_y,a_x) = turn               #usually Y=car, X=ped
            #only consider interesting game states, within Q boxes to clear overlap around collision point
            if y==x or y==(x-1) or y==(x+1) or y==(x-2) or y==(x+2) or (y==x-3) or y==(x+3) or (y==x+4) or (y==x-4):        #only consider interesting game states
            #if y==x:        #only consider interesting game states
                if a_x == 1: #did ped yield?
                    hist_slow[x]+=1
                if a_x == 2:
                    hist_fast[x]+=1
    return (hist_slow, hist_fast)

