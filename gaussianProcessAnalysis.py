#analyse and plot results as gaussian process

import GPy, numpy as np
import matplotlib.pyplot as plt
from fit_data import *
from pylab import *


def gplot(results, title):
    
    # extract u_crash, u_time, likelihood from list of tuples as X, Y, Z
    UC = np.array([x[0] for x in results])   # u_crash
    UT = np.array([x[1] for x in results])   # u_time
    LL = np.array([x[2] for x in results])   #likelihood
    maxLL = max(results, key=operator.itemgetter(2))  # max of LL
    X = np.vstack((UC, UT)).T    #the 2D coordinates on the graph
    Y = np.vstack((LL))          #the values observed at these coordinates
    
    #fit the GP 
    ker = GPy.kern.RBF(input_dim=2, ARD = True,  variance = 1/100., lengthscale= 10.)
    m = GPy.models.GPRegression(X,Y,ker)
    m.optimize(messages=True,max_f_eval = 1000)

    #plot the GP contours 
    m.plot()
    plt.title(title + " -- Optimum: " + str((maxLL[0], maxLL[1], round(maxLL[2], 4))))
    plt.xlabel("U_crash")
    plt.ylabel("U_time")
    show()
    
    #plot some slices of the GP
    slices = [-1,0, 1.5]
    figure = GPy.plotting.plotting_library().figure(3,1)
    for i, y in zip(range(3), slices):
        m.plot(figure=figure, fixed_inputs=[(1,y)], row=(i+1), plot_data=False)
    plt.title(title)
    plt.xlabel('U_crash')
    plt.ylabel('Loglik')
    show()
    
   
