# load data from standard discrete CSV format (defined by MeasuringBehviour data example)
# into standard discrete game list of list format

import numpy as np
import pandas as pd
import operator
import string 
import pdb

def load_data(filename):
	games = []
	b_headers=True
	for line in open(filename):
		if b_headers:
			b_headers=False
			continue
		line=line.strip()	
		fields = line.split(",")
		if len(fields)<5:
			print("bad row")
			continue

		metadata0 = fields[0]
		metadata1 = fields[1]
		metadata2 = fields[2]
		metadata3 = fields[3]
		
		states = fields[4:]

		game = []
		for state in states:
			state=state.strip()
			if len(state)<1:
				continue
			vals = state.split(" ")
			if len(vals)<1:
				continue
			a_y = int(vals[0])
			y = int(vals[1])
			a_x = int(vals[2])
			x = int(vals[3])

			if x>-1 and y>-1:	
				game.append((y,x, a_y,a_x))	
		games.append(game)
	return games
    
if __name__ == "__main__":
    games = load_data('board_game_data.csv')
    print(games)

	
