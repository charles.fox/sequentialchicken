#convert data from continous CSV standard format (defined by HFIV2020 data example)
#to discrete game list of list standard format

import os, sys
import numpy as np
import pandas as pd
import operator
import string 
import pdb

def load_data(path, ds, dt, vp):
    dirs = os.listdir( path )
    games_number = [1, 6, 8, 11, 12, 13, 14, 15, 16, 22, 23, 24, 30, 32]
    games = []
    for num in games_number:       
        game = []
        b_headers=True
        x_prev = 6
        y_prev = 6
        x_mean = 0
        y_mean = 0
        with open(path + '/' + "natural" + str(num) + "_playerX.csv") as x_file, open(path + '/' + "natural" + str(num) + "_playerY.csv") as y_file: 
            counter = 0
            for x_line, y_line in zip(x_file, y_file):
                if b_headers:
                    b_headers=False
                    continue
                counter = counter + 1
                x_line= x_line.strip()
                x_states = x_line.split(",")
                y_line= y_line.strip()
                y_states = y_line.split(",")
                if x_states[1] == y_states[1]:  # only consider positions where players have same timestamps
                    x = float(x_states[3])
                    y = float(y_states[4])
                    x_mean = x_mean + x
                    y_mean = y_mean + y
                    if counter %3 == 0:     
                        x_mean = x_mean/3.0
                        y_mean = y_mean/3.0         
                        #print("dx = " + str(x_prev - x_mean))
                        if (x_prev - x_mean) > vp*dt:
                            a_x = 2
                        else:
                            a_x = 1
                        #print("dy = " + str(y_prev - y_mean))
                        if (y_prev - y_mean) > vp*dt:
                            a_y = 2
                        else:
                            a_y = 1
                        x_prev = x_mean
                        y_prev = y_mean
                        if x_mean>0.0001 and y_mean>0.0001:
                            X = int(x_mean/ds)   # convert distance from meters to cell number
                            Y = int(y_mean/ds)   # same as above
                            game.append((Y, X,a_y,a_x))
                            x_mean = 0
                            y_mean = 0
                        else:
                            break;
                                            
            games.append(game)  
    return games
    
