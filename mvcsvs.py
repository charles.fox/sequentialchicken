

import os, re
import pdb
import multiprocessing, sys, subprocess


directory = "/home/charles/docs/rak/bags"



for file in sorted(os.listdir(directory)):
    filename_day = os.fsdecode(file)
    dir_day = os.path.join(directory, filename_day)
    print(dir_day)
    for file in sorted(os.listdir(dir_day)):
        filename_subject = os.fsdecode(file)
        dir_subject = os.path.join(dir_day, filename_subject)
        print(dir_subject)
        
        for file in sorted(os.listdir(dir_subject)):
            filename_crossing = os.fsdecode(file)
            dir_crossing = os.path.join(dir_subject, filename_crossing)
            print(dir_crossing)
        
            for file in sorted(os.listdir(dir_crossing)):
                filename_crossing = os.fsdecode(file)
                fn_crossing = os.path.join(dir_crossing, filename_crossing)
                short_filename_subject = re.sub(r"^.*-", "", filename_subject)

                if fn_crossing[-3:] == "csv":
                    
                    fn_csv_new = "csv/%s_%s_%s"%(filename_day, short_filename_subject, filename_crossing)        
                    
                    cmd = "cp %s %s"%(fn_crossing, fn_csv_new)
                        
                    subprocess.run([cmd], shell=True)
