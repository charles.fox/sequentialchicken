import multiprocessing
import subprocess

def system_call_wrapper(command, stop_event):
  # Wait on the stop_event if set before continuing
  stop_event.wait()
  subprocess.run([command])

if __name__ == "__main__":
  stop_event = multiprocessing.Event()  # Create an event object

  process1 = multiprocessing.Process(target=system_call_wrapper, args=("touch a", stop_event))
  process2 = multiprocessing.Process(target=system_call_wrapper, args=("touch b", stop_event))

  process1.start()
  process2.start()

  # Wait for process1 (the first call) to finish
  process1.join()

  # Signal termination to process2 (the second call)
  stop_event.set()

  # Wait for process2 to potentially finish any remaining work before termination
  process2.join(timeout=1)  # Optional timeout to avoid hanging

  print("System call 1 finished. System call 2 terminated.")
