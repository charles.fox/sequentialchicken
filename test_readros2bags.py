#pip install rosbags

from rosbags.rosbag2 import Reader

from rosbags.serde import deserialize_cdr
from rosbags.typesys import get_types_from_msg, get_types_from_idl, register_types


# create reader instance and open for reading
fn_bag = "/home/charles/docs/rak/bags/day1/20240607-Sub2/cross_1/"


with open("/home/charles/docs/rak/GameInfo.msg", "r") as file:
  # Read the entire content into a string variable
  data = file.read()


message_text = get_types_from_msg(data, "pod2_msgs/msg/GameInfo")
register_types(message_text)


with Reader(fn_bag) as reader:
    # topic and msgtype information is available on .connections list
    for connection in reader.connections:
        print(connection.topic, connection.msgtype)

    # iterate over messages
    for connection, timestamp, rawdata in reader.messages():
        if connection.topic == '/chicken_game_info':
#        if connection.topic == '/cmd_vel_chicken_modulated':

            msg_type = connection.msgtype
            print(msg_type)

            msg = deserialize_cdr(rawdata, connection.msgtype)
#            print(msg.header.frame_id)
            print(msg)
