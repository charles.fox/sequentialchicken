#solving a 2x2 zero sum game
#without nashpy, using maths from Owen p26.

#A = np.matrix([[1,0],[-1, 2]])   #game matrix that works
A = np.matrix([[-1,1],[1, -1]])   #game matrix causing singular matrix error
J = np.matrix([[1,1]])   #boilerplate

#optimal strategies
x = np.dot(J,A.I) / ( np.dot( np.dot(J, A.I) , J.T ) )
y = np.dot(A.I, J.T) / ( np.dot( np.dot(J, A.I) , J.T ) )

#value of the game (expected how much player 2 gives to player 1)
v = 1./ ( np.dot( np.dot(J, A.I) , J.T ) )

#TODO if A is singular, use adjoint instead of inverse ?
