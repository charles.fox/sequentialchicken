#unit tests
#pip3 install nashpy

import nashpy
import numpy as np
import sequentialChicken

def test_nashpy_tutorial_rockPaperScissors():
    print("TESTING nashpy rockPaperScissors tutorial example from https://nashpy.readthedocs.io/en/stable/tutorial/")
    A = np.array([[0, -1, 1], [1, 0, -1], [-1, 1, 0]])   #player A's utilities for scissors, paper, stone
    B=-A   #zero sum game, so player B's utilies are -A
    rps = nashpy.Game(A, B)
    print("deterministic strategy for both players")
    sigma_rowPlayer = [0, 0, 1]   #deterministic strategy for row player 
    sigma_colPlayer = [0, 1, 0]   #deterministic strategy for col player
    utils = rps[sigma_rowPlayer, sigma_colPlayer]
    print(utils)
    assert(utils[0]==1)
    assert(utils[1]==-1)
    print("now try with a random strategy")
    sigma_rowPlayer = [0., 1/2, 1/2]
    utils = rps[sigma_rowPlayer, sigma_colPlayer]   #get values of the game for the two players under their strategies
    print(utils)
    assert(utils[0]==0.5)
    assert(utils[1]==-0.5)
    print("find the equilibria using support enumeration method")
    eqs = rps.support_enumeration()    
    eqs_list = list(eqs)
    print(eqs_list)
    assert(len(eqs_list)==1) #there is only one equilibrium in rockPaperScissors
    eq0 = eqs_list[0]   #select the first (only) equilibrium
    eq_rowPlayer = eq0[0]
    print(eq_rowPlayer)
    eq_colPlayer = eq0[1]
    print("check that the optimal strategy is flat for both players")
    assert(eq_rowPlayer[0]==1./3)
    assert(eq_rowPlayer[1]==1./3)
    assert(eq_rowPlayer[2]==1./3)
    assert(eq_colPlayer[0]==1./3)
    assert(eq_colPlayer[1]==1./3)
    assert(eq_colPlayer[2]==1./3)

    print("compute same equibirium using vertex enumeration method")
    print("TODO why does this appear to give three equibiria?")
    #second is the correct one, both players have flat strategies
    #first is wrong, shows both players player stone with P=1 ?
    eqs_vertex = rps.vertex_enumeration()    
    for eq in eqs_vertex:
        print(eq)
#   eqs_vertex_list = list(eqs_vertex)
#    print(eqs_vertex)

    print("compute equibiria using Lemke-Howson, NB this not not promise to find all equilibria, only one or some of them")
    eqs_lemkehowson = rps.lemke_howson(initial_dropped_label=0) 
    for eq in eqs_lemkehowson:
        print(eq)


def test_sequentialChicken_2timeSteps_sym():
    print("TESTING sequentialchicken_2timeSteps_sym")
    (V,S) = sequentialChicken.solveGame(U_crash_Y=-100, U_crash_X=-100, U_time=1., NY=2, NX=2)
    #V is an NX*NY matrix with elements containing utility pairs for the two players, (U_x, U_y)
    #S are the strategies for the players, 0=slow 1=fast
    print(V)
    print("check that U_X for position [x=0, x=0] is the crash penalty")
    X=0; Y=1   #int IDs for the two players
    x=0; y=0;  #positions as int squares on the grid 
    assert(V[x,y][X] == -100.)   
    print("check that U_Y for position [X=0, Y=0] is the crash penalty")
    assert(V[x,y][Y] == -100.)   
    print("check that value to row (X) player of (x=0, y=1) is -0.5, (because X could travel at speed FAST=2, taking 1/2 a tick to complete.)")
    x=1; y=0;  #positions as int squares on the grid 
    assert(V[x,y][X] == -0.5)   
    print("check that value to col (Y) player of (x=0, y=1) is 0, (because Y has arrived with no delay.)")
    assert(V[x,y][Y] == 0.)   

def test_sequentialChicken_3timeSteps_sym():
    #TODO add some assertions about this game 
    V = sequentialChicken.solveGame(U_crash_Y=-100, U_crash_X=-100, U_time=1., NY=3, NX=3)
    V_playerX = V[0]   #row player
    V_playerY = V[1]   #col player
    print(V)


def test_solveGame_vehits():
    U_crash = -5.
    (V,S) = sequentialChicken.solveGame(U_crash, U_crash, NY=20, NX=20)
    assert(V[0,0][0] == -5.)
    assert(V[0,0][1] == -5.)
    assert(V[0,2][0] == 0.)
    assert(V[0,2][1] == -1.)
    assert(V[0,5][0] == 0.)
    assert(V[0,5][1] == -2.5)
    assert(V[1,1][0] == -5.)
    assert(V[1,1][1] == -5.)
    assert(V[9,1][0] == -4.5)
    assert(V[9,1][1] == -0.5)
    assert(np.ceil(V[9,9][0]) == -5.)
    assert(np.ceil(V[9,9][1]) == -5.)
    assert(np.ceil(V[19,19][0]) == -10.)
    assert(np.ceil(V[19,19][1]) == -10.)

    #improtant - these are what went wrong when I tried to refactor solution concepts
    assert(S[0,0,1] == 0.0)
    assert(S[1,1,1] == 0.0)
    assert(S[2,2,1] == 0.47368421052631576)
    assert(S[10,10,1] == 0.14156718277015923)
    assert(S[18,18,1] == 0.010493116523609977)
    assert(S[19,19,1] == 0.007483408851858428)


if __name__=="__main__":
    test_nashpy_tutorial_rockPaperScissors()
    test_sequentialChicken_2timeSteps_sym()
    test_solveGame_vehits()
